__Hyper parameter tunning of OPF__

# DUDAS

0. creo que cbc se llama el solver mas gratis y facil de instalar, y de ahi le busco el parametro 'warmstart' o algo asi para que pesque la inicializacion de solucion que quieren verificar. creo que el objetivo podria ser un numero constante o simplemente minimizar el flujo... ?en una de esas funciona sin objetivo?

1. Por que' usa este metodo DC para una red AC ?
	en linea 241 del archivo `analysis.py`, usa `rundcpp`
	en la funcion `def mo_opf(ser_decisions, net):` 
	en vez de `runpp` ?
	- https://pandapower.readthedocs.io/en/v2.4.0/powerflow/dc.html
	- https://pandapower.readthedocs.io/en/v2.4.0/powerflow/ac.html
	- https://pandapower.readthedocs.io/en/v2.4.0/powerflow/run.html

2. Son 100001 filas para resultado.csv ? suena sospechoso ese numero... a que se supero la version gratis de un modulo

3. ? Seguro que no hay otro metodo mejor que llenar unas listas de np.nan ? parece que estuviesen programando en C con arreglos estaticos...
```
temp = [np.nan]*len(df)
temp_F_cos = [np.nan]*len(df)
...
```
algo asi?
```
temp_F_cos=[]
for i, row in df.iterrows():
	...
        temp_F_cos += [F_cos(pg)]
```
en verdad si lo pienso un poco mejor, toda la gracia de pandas es nunca iterar por fila
```
# se puede calcular algo asi
df['suma'] = df['~1'] + df...(el nombre verdadero de la columna)...[4] + df[7] + df[10] + df[12]

# y filtrar
df['temp'] = df['suma'].apply(lambda x: np.nan if x > 283.4 else 283.4-x)
# o algo asi
df = df.assign(temp = lambda x: ...)

# finalmente ... en una de esas funciona algo asi
df['F_cos'] = df[('1','4...)].apply(temp_F_cos)
```

# Instance
## Toy
## chile 
## benchmark

# Pseudo code
Mathematical Programming : Optimal Power Flow
```

sets
    nodes ~ generator or sink
    edges ~ Line flow

variables
	p [g, j] ~ power generated at node g, flowing to node j Forall g,j in N

objective
    min F( sum(g in G) p[g] )

    F = f_cos + f_emit + f_with + f_con

	# total fuel cost
        f_cos  = sum( j in G) a[j] + b[j] * p[g,j] + c[j] * p[g,j]**2

	# total emissions
        f_emit = sum( j in G) 0.01 * ( alpha[j] + beta_emit[j] * p[g,j] + gamma[j] * p[g,j]**2 ) + epsilon[j]* exp( lamda[j] * p[g,j] )

	# water withdrawn (removed from water body)
	f_with = sum( j in G) beta_with[j] * p[g,j]

	# water_consumed (not returned to the same water body)
	f_con = sum( j in G) beta_con[j] * p[g,j]

constraints
	- Fmax[im] <= B[im] * theta[im] <= Fmax[im] , forall im in Lines

	- p[l,i] - sum(k in G[i]) p[g,k] = sum( m in N) B[im] * theta[im]  for all i in N , l in Lines

	- p_lb[g,j] <= p[g,j] <= p_ub[g,j]

```
