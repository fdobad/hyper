from pyomo.environ import *
'''
TODO transformarlo en concreto y encapsularlo pasandole los valores iniciales
'''

model = AbstractModel()
# Nodes in the network
model.N = Set()
# Network arcs
model.A = Set(within=model.N*model.N)
# Source node
model.s = Param(within=model.N)
# Sink node
model.t = Param(within=model.N)
# Flow capacity limits
model.c = Param(model.A)

# The flow over each arc
model.f = Var(model.A, within=NonNegativeReals)

# Enforce an upper limit on the flow across each arc
def limit_rule(model, i, j):
    return model.f[i,j] <= model.c[i, j]
model.limit = Constraint(model.A, rule=limit_rule)

# Enforce flow through each node
    #if k == value(model.s) or k == value(model.t):
    #    return Constraint.Skip
def flow_rule(model, k):
    inFlow  = sum(model.f[i,j] for (i,j) in model.A if j == k)
    outFlow = sum(model.f[i,j] for (i,j) in model.A if i == k)
    sum( model.B[im] * model.theta[im] )
    return inFlow == outFlow
model.flow = Constraint(model.N, rule=flow_rule)

# Maximize the flow into the sink nodes
def total_rule(model):
    return sum(model.f[i,j] for (i, j) in model.A if j == value(model.t))
model.total = Objective(rule=total_rule, sense=maximize)
