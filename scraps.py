#
# paper
#
    # Inputs
    dec_labs = ['1', '4', '7', '10', '12', '0']
    obj_labs = ['F_cos', 'F_emit', 'F_with', 'F_con']
    obj_epsi = [10.0, 0.01, 100000.00, 10000.00]
    dec_labs_pretty = ['Gen ' + i + ' (MW)' for i in dec_labs]
    obj_labs_pretty = [
        'Cost ($/hr)',
        'Emissions (ton/hr)',
        'Withdrawal (gal/hr)',
        'Consumption (gal/hr)'
    ]
    inputs = analysis.input_parse()
    # Params
    model.eps = pyo.Param(initialize=1)
    model.a = pyo.Param(model.E, initialize=1)
    model.b = pyo.Param(model.E, initialize=1)
    model.c = pyo.Param(model.E, initialize=1)
    
        # Emissions
        # Withdrawal
    model.'F_cos'
        df['F_emit'] = \
        df['F_with'] = df['beta_with'] * df['p_mw']
    = \
            df['a'] + df['b'] * (df['p_mw'] / 100) + \
            df['c'] * (df['p_mw'] / 100) ** 2
    
            0.01 * df['alpha'] + \
            0.01 * df['beta_emit'] * (df['p_mw'] / 100) + \
            0.01 * df['gamma'] * (df['p_mw'] / 100) ** 2 + \
            df['xi'] * np.exp(df['lambda'] * (df['p_mw'] / 100))
        # Consumption
    model.F_con = pyo.Var()
    mode.beta_con model.p_mw[I,J]
    
    @model.Constraint(model.n)
    def RightBound(m, i):
        j = np.argmax(initialPositions[i,:,0])
        print('i,j',i,j)
        return m.x[i,j] <= m.Rbb 

#
# pyomo
#
def solver():
    with SolverFactory("ipopt") as opt:
        model = create_model()
        results = opt.solve(model, load_solutions=False)
        if results.solver.termination_condition != TerminationCondition.optimal:
            raise RuntimeError('Solver did not report optimality:\n%s'
                               % (results.solver))
        model.solutions.load_from(results)
        print("Objective: %s" % (model.o()))
#
# modules shit
#
import matplotlib.pyplot as plt
import numpy as np

import networkx as nx

from pyomo.environ import *
from multi_objective_opf.src import analysis
import pandapower as pp
import pymoo.util.nds.efficient_non_dominated_sort as ends
#from pareto import pareto as pto
#import hiplot as hip

#
# graph SHIT
#
def toy():
    nodes = list(map( lambda x: 'n'+str(x), range(4) ))
    edges = np.random.choice( nodes, size=(4,2))
    edges = [['n0','n1'],['n0','n2'],['n0','n3'],\
             ['n1','n0'],['n1','n2'],\
             ['n2','n0'],['n2','n1'],['n2','n3'],\
             ['n3','n0'],['n3','n2'],\
             ]
    G = nx.DiGraph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    nx.draw(G, with_labels=True, font_weight='bold')
    plt.show(block=False)
    #plt.clf()
    return G,nodes,edges

n=5
nodes = list(map( lambda x: 'n'+str(x), range(n) ))
edges = np.random.choice( nodes, size=(n,2))
#weights = np.random.randint(1,10,len(edges))
weights = np.round( np.random.rand(len(edges)), 1)

edges = np.array([['n2', 'n1'],['n1', 'n0'],['n3', 'n0'],['n4', 'n2'],['n0', 'n1'],['n2', 'n0']], dtype='<U2')
nodes = np.array(['n0', 'n1', 'n2', 'n3', 'n4'], dtype='<U2')
weights = np.round( np.random.rand(len(edges)), 1)

# Create the 3D figure
fig = plt.figure()
ax = fig.add_subplot(111)#, projection="3d")

G = nx.DiGraph()
#G = nx.Graph()

G.add_weighted_edges_from( [ (*e,str(weights[i])) for i,e in enumerate(edges) ] )
#G.add_nodes_from(nodes)
#G.add_edges_from(edges)

#pos
#pos = nx.spring_layout(G)
pos = nx.spring_layout(G, k=10)  # For better example looking

# v0
edge_labels = dict([((n1, n2), d['weight']) for n1, n2, d in G.edges(data=True)])
# v1
labels = {e: G.edges[e]['weight'] for e in G.edges}
nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
# v2
edge_labels = nx.get_edge_attributes(G,'weight') # key is edge, pls check for your case
formatted_edge_labels = {(elem[0],elem[1]):edge_labels[elem] for elem in edge_labels} # use this to modify the tuple keyed dict if it has > 2 elements, else ignore
nx.draw_networkx_edge_labels(G,pos,edge_labels=formatted_edge_labels)

#draw
nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, label_pos=0.9, font_color='red', font_size=16, font_weight='bold')
nx.draw(G, with_labels=True, node_color='skyblue', edge_cmap=plt.cm.Blues, pos = pos)
nx.draw(G, pos, with_labels=True, arrows=True)

plt.draw()
plt.show()

G = nx.Graph()
G.add_edge(1, 2, color="red")
G.add_edge(2, 3, color="red")
G.add_node(3)
G.add_node(4)

A = nx.nx_agraph.to_agraph(G)  # convert to a graphviz graph
A.draw("attributes.png", prog="neato")  # Draw with pygraphviz

# convert back to networkx Graph with attributes on edges and
# default attributes as dictionary data
X = nx.nx_agraph.from_agraph(A)
print("edges")
print(list(X.edges(data=True)))
print("default graph attributes")
print(X.graph)
print("node node attributes")
print(X.nodes.data(True))
