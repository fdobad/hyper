from pyomo import environ as pyo
import numpy as np
model = pyo.ConcreteModel()
#
# conjuntos
#
# m restricciones
m = 4
model.M = pyo.Set( initialize=range(m))
# n decisiones
n = 4
model.N = pyo.Set( initialize=range(n))
#
# parametros
# costo
model.c = pyo.Param( model.N, initialize = dict(zip(range(n), np.random.random(size=n)*100)) )
# vector restriccion
model.b = pyo.Param( model.M, initialize = dict(zip(range(m), np.random.random(size=m)*100)))
# matriz restriccion
# llenar diccionario con valores
values = dict()
for i in range(m):
    for j in range(n):
        values[i,j] = np.random.random()
for i in range(n):
    for j in range(m):
        values[j,i] = np.random.random()
model.A = pyo.Param( model.M, model.N, initialize = values )
#
# variable de decision
model.x = pyo.Var(model.N, domain=pyo.Reals, initialize=dict(zip(range(n),np.zeros(n))))
#
# restriccion
def constraint_rule(model, k):
    return sum( model.A[j,k] * model.x[j] for j in model.N ) == model.b[k]
model.constraint = pyo.Constraint( model.N, rule=constraint_rule)

def sinSentido():
    if np.random.rand()<0.5:
        return 1
    else:
        return -1
#
# funcion objetivo
def total_rule(model):
    return sum( sinSentido() * model.x[i]*model.x[i] * model.c[i] for i in model.N)
model.total = pyo.Objective(rule=total_rule, sense=pyo.maximize)

# resolver
solver = pyo.SolverFactory('ipopt')
solved = solver.solve(model, tee=True)#, options_string="warm_start_init_point=yes")

# mostrar
#[ pyo.value(v) for v in model.x.values() ]
print('objetivo', model.total() )
print('decisiones',[ (k,pyo.value(v)) for k,v in model.x.items()])

