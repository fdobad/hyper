import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import itertools


def grid_sample(df_gridspecs):
    """
    Pandas-based grid sampling function
    Parameters
    ----------
    df_gridspecs: DataFrame
        Grid specifications, must have columns of
        ['var', 'min', 'max', 'steps']. These reflect the variable names,
        minimum value of grid sampling, maximum value of grid sampling,
        and number of steps respectively.
    Returns
    -------
    df_grid: DataFrame
        Dataframe of grid sampling. Will have columns of names specified in
        'var' list
    """
    # Get linear spaces
    linspace_list = []
    for i, row in df_gridspecs.iterrows():
        linspace_list.append(
            np.linspace(row['min'], row['max'], int(row['steps']))
        )

    # Create dataframe
    df_grid = pd.DataFrame(
        list(itertools.product(*linspace_list)),
        columns=df_gridspecs['var'].tolist()
    )

    return df_grid


df_gridspecs = pd.DataFrame(
    {
        'var': [1, 4, 7, 10, 12], #nombres de las variables
        'min': [0., 0., 0., 0., 0.], #minimos de los generadores
        'max': [140., 100., 100., 100., 100.],#maximos de los generadores
        'steps': [10, 10, 10, 10, 10]#steps usados para los linspace
    }
)
# df_parameters = pd.DataFrame(
#     {
#         'var': [1, 4, 7, 10, 12, 0],
#         'a': [10., 20., 10., 20., 10., 10.],
#         'b': [150., 180., 100., 180., 150., 200.],
#         'c': [120., 40., 60., 40., 100., 100.],
#         'alpha': [0., 4.258, 5.426, 4.258, 6.131, 4.091],
#         'beta_emit': [0., -5.094, -3.55, -5.094, -5.555, -5.554],
#         'gamma': [0., 4.586, 3.38, 4.586, 5.151, 6.49],
#         'xi': [0., 1e-6, 0.002, 1e-6, 1e-5, 0.0002],
#         'lambda': [0., 8., 2., 8., 6., 2.857],
#         'beta_with': [44350., 225., 36350., 36350., 11380., 225.],
#         'beta_con': [269., 826., 250., 250., 100., 826.]
#     }
# )

df_parameters = pd.DataFrame(
    {
        'var': [1, 4, 7, 10, 12, 0],
        'a': [10., 20., 10., 20., 10., 10.],
        'b': [150., 180., 100., 180., 150., 200.],
        'c': [120., 40., 60., 40., 100., 100.],
        'alpha': [0., 4.258, 5.426, 4.258, 6.131, 4.091],
        'beta_emit': [0., -5.094, -3.55, -5.094, -5.555, -5.554],
        'gamma': [0., 4.586, 3.38, 4.586, 5.151, 6.49],
        'xi': [0., 1e-6, 0.002, 1e-6, 1e-5, 0.0002],
        'lambda': [0., 8., 2., 8., 6., 2.857],
        'beta_with': [44350., 225., 36350., 11380., 36350., 225.],
        'beta_con': [269., 826., 250., 100., 250., 826.]
    }
)

def F_cos(pg):
    output = np.sum(df_parameters['a'] + df_parameters['b']*pg/100 + df_parameters['c']*[(pg_v/100)**2 for pg_v in pg])
    return output

def F_emit(pg):
    output = np.sum(1e-2*df_parameters['alpha'] + 
                    1e-2*df_parameters['beta_emit']*pg/100 + 
                    1e-2*df_parameters['gamma']*[(pg_v/100)**2 for pg_v in pg] +
                    df_parameters['xi']*np.exp(df_parameters['lambda']*pg/100))
    return output

def F_with(pg):
    output = np.sum(df_parameters['beta_with']*pg)
    return output


def F_con(pg):
    output = np.sum(df_parameters['beta_con']*pg)
    return output

def isFactible(pg):
    cons1 = True
    output = cons1
    return output

df = grid_sample(df_gridspecs)

#aqui esta harcodeao el 283.4 
# ES IMPORTANTE CAMBIAR ESTO A LA DEMANDA TOTAL EN EL CASO CHILENO. 
#se queda pegado aqui??
temp = [np.nan]*len(df)
temp_F_cos = [np.nan]*len(df)
temp_F_emit = [np.nan]*len(df)
temp_F_with = [np.nan]*len(df)
temp_F_con = [np.nan]*len(df)

for i, row in df.iterrows():
    suma = (row[1] + row[4] + row[7] + row[10] + row[12])
    if suma > 283.4:
        temp[i] = np.nan
    else:
        temp[i] = 283.4 - suma
        pg = [row[1], row[4], row[7], row[10], row[12], temp[i]]
        temp_F_cos[i] = F_cos(pg)
        temp_F_emit[i] = F_emit(pg)
        temp_F_with[i] = F_with(pg)
        temp_F_con[i] = F_con(pg)



df['0']= temp
df['F_cos'] = temp_F_cos
df['F_emit'] = temp_F_emit
df['F_with'] = temp_F_with
df['F_con'] = temp_F_con

df_csv_data = df.to_csv('resultados.csv', index = False)

