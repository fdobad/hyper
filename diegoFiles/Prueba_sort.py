import pymoo.util.nds.efficient_non_dominated_sort as ends
import numpy as np
import pandas as pd
import pandapower as pp
import pareto as pto
obj_labs = ['F_cos', 'F_emit', 'F_with', 'F_con']
obj_epsi = [10.0, 0.01, 100000.00, 10000.00]

def get_nondomintated(df, objs, max_objs=None):
    """
    Get nondominated filtered DataFrame

    Parameters
    ----------
    df: DataFrame
        DataFrame for nondomination
    objs: list
        List of strings correspond to column names of objectives
    max_objs: list (Optional)
        List of objective to maximize

    Returns
    -------
    df_nondom: DataFrame
        Nondominatated DataFrame
    """
    # Create temporary dataframe for sorting
    df_sort = df.copy()

    # Flip objectives to maximize
    if max_objs is not None:
        df_sort[max_objs] = -1.0 * df_sort[max_objs]

    # Non-dominated sorting
    nondom_idx = ends.efficient_non_dominated_sort(df_sort[objs].values)
    df_nondom = df.iloc[nondom_idx[0]].sort_index()

    return df_nondom

def get_epsilon_nondomintated(df, objs, epsilons, max_objs=None):
    """
    Get epsilon nondominated filtered DataFrame

    Parameters
    ----------
    df: DataFrame
        DataFrame for nondomination
    objs: list
        List of strings correspond to column names of objectives
    epsilons: list
        List of floats specifying epsilons for pareto sorting
    max_objs: list (Optional)
        List of objective to maximize
    Returns
    -------
    df_nondom: DataFrame
        Nondominatated DataFrame
    """
    # Get indices of objectives
    objs_ind = [df.columns.get_loc(col) for col in objs]
    try:
        max_ind = [df.columns.get_loc(col) for col in max_objs]
    except TypeError:
        max_ind = None

    # Nondominated sorting
    non_dominated = pto.eps_sort(
        [list(df.itertuples(False))], objs_ind, epsilons, maximize=max_ind
    )

    # To DataFrame
    df_nondom = pd.DataFrame(non_dominated, columns=df.columns)

    # Get original index
    df_nondom = df.reset_index().merge(df_nondom).set_index('index')
    df_nondom.index.name = None

    return df_nondom


df_grid_results = pd.read_csv("resultados_chile.csv")

# Drop na
df_grid_results = df_grid_results.dropna()


df_nondom = get_epsilon_nondomintated(
        df_grid_results,
        objs=obj_labs,
        epsilons=obj_epsi
    )

#df_nondom.to_excel("output_Chile_V8.xlsx",sheet_name='Sheet_name_1')
print(df_nondom[["F_cos","F_emit","F_with","F_con"]].corr())