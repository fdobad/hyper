"""Conduct analysis python script"""

########## IMPORTACIONES 
## OS == libreria estandar de Python para realizar funciones de acceso,
##       modificaciones de directorios.
import os

## pandapower == libreria utilizada para SEP diseñada en PYTHON de codigo abierto
## pandapower.networks == da acceso a las redes de prueba de pandapower y redes
##                        comunmente utilizadas en USA.
import pandapower.networks
## pandas == libreria de Python para manejar, modificar, analizar y abrir datos
import pandas as pd
## dask.dataframe == es un data frame largo compuesto de muchos dataframes
##                   pequeños de pandas, unidos por medio de indices.
import dask.dataframe as dd
## numpy == es una libreria de python estandar para operaciones matematicas
import numpy as np
## Desde el directorio del computador se importa ANALYSIS
from src import analysis
## Desde el directorio del computador se importa VIZ
from src import viz



        ######################################################################
        ######################################################################
        ######################################################################
####################### INICIO DE LA FUNCION PARA CORRER EL PROGRAMA
def main():
    
        ######################################################################
        ######################################################################
        ######################################################################
    # Definir "Inputs"
    #### Barras donde van los GENERADORES 
    dec_labs = ['1', '4', '7', '10', '12', '0']
    ####  COEFICIENTES DE cos(tos), emit(tions), with(drawn), con(ssution)
    obj_labs = ['F_cos', 'F_emit', 'F_with', 'F_con']
    #### EPSILON PARA convergencia del programa
    obj_epsi = [10.0, 0.01, 100000.00, 10000.00]
    #### Nombrar los generadores Gen1(MW), Gen4, Gen7, Gen10, Gen12, Gen0
    dec_labs_pretty = ['Gen ' + i + ' (MW)' for i in dec_labs]
    #### Se define el vector de Objetivos F_Cost, F_Emissions, F_Withdrawal, F_Consumption
    obj_labs_pretty = ['Cost ($/hr)',
                       'Emissions (ton/hr)',
                       'Withdrawal (gal/hr)',
                       'Consumption (gal/hr)']
   
        ######################################################################
        ######################################################################
        ######################################################################    
    # Se define los IMPUTS para correr el programa usando la funcion ANALYSIS
    inputs = analysis.input_parse()
    """  
    def input_parse(path_to_config=None):
        Parameters
        path_to_config: str, Path to config file (gets preference over command line)
        Returns Dictionary of inputs
    
        # Local vars
        config_inputs = configparser.ConfigParser()
        argparse_inputs = argparse.ArgumentParser()
        if path_to_config:
            config_inputs.read(path_to_config)
        else:
            ###
            ### Command line arguments (these get priority)
            argparse_inputs.add_argument('-c','--config_file',type=str,action='store',help='Path to configuration file',required=True)
            argparse_inputs.add_argument('-nt','--n_tasks',type=int,default=2,action='store',help='Number of tasks for parallelization',required=False)
            argparse_inputs.add_argument('-ns','--n_steps',type=int,default=10,action='store',help='Number of steps for grid search',required=False)
            ###
            ### Parse arguments
            argparse_inputs = argparse_inputs.parse_args()
            ###
            ### Parse config file
            config_inputs.read(argparse_inputs.config_file)
        ###
        ### Store inputs
        path_to_data = config_inputs['MAIN IO']['data']
        inputs = {'path_to_data': path_to_data,
                  ### 
                  ### ABIDO COEFICIENTES
                  'path_to_df_abido_coef': os.path.join(path_to_data,config_inputs['INPUT']['path_to_df_abido_coef']),
                  ### 
                  ### MACNINK COEFICIENTES
                  'path_to_df_macknick_coef': os.path.join(path_to_data,config_inputs['INPUT']['path_to_df_macknick_coef']),
                  ###
                  ### RESULTADOS DE LA RED, GRID_RESULTS
                  'path_to_df_grid_results': os.path.join(path_to_data,config_inputs['GENERATED_FILES']['path_to_df_grid_results']),
                  ###
                  ###
                  'path_to_df_nondom': os.path.join(path_to_data,config_inputs['GENERATED_FILES']['path_to_df_nondom']),
                  'n_tasks': argparse_inputs.n_tasks,
                  'n_steps': argparse_inputs.n_steps,
                  ###
                  ### NONDOM OBJETIVEZ
                  'path_to_nondom_objectives_viz': os.path.join(path_to_data,config_inputs['FIGURES']['path_to_nondom_objectives_viz']),
                  ###
                  ### NONDOM DECISIONS
                  'path_to_nondom_decisions_viz': os.path.join(path_to_data,config_inputs['FIGURES']['path_to_nondom_decisions_viz']),
                  ###
                  ### MATRIZ DE CORRELACION
                  'path_to_objective_correlation_viz': os.path.join(path_to_data,config_inputs['FIGURES']['path_to_objective_correlation_viz']),
                  ###
                  ### PLOT DE LOS NONDOM
                  'path_to_nondom_hiplot_viz': os.path.join(path_to_data,config_inputs['FIGURES']['path_to_nondom_hiplot_viz'])
                 }
        return inputs    
      """
        ######################################################################
        ######################################################################
        ######################################################################
    ### if not false:
    ###   print('hola')
    ### ---> hola
    ### if not True: 
    ### ---> None
    ###
    ############# if not os.path.exists(inputs['path_to_df_grid_results'])==true:
    ############# -----> No ejecutara lineas
    ############# if not os.path.exists(inputs['path_to_df_grid_results'])==FALSE: 
    ############# -----> Ejecutara Lineas
    if not os.path.exists(inputs['path_to_df_grid_results']):
        ##
        ## Se extrae de PandaPower la red IEEE CASO 30 (case_ieee30)
        net = pandapower.networks.case_ieee30()
        ## 
        ## Se extraen los COEFICIENTES DE ABIDO del CSV 
        df_abido_coef = pd.read_csv(inputs['path_to_df_abido_coef'])
        ## 
        ## Se extraen los COEFICIENTES DE MACKNICK del CSV 
        df_macknick_coef = pd.read_csv(inputs['path_to_df_macknick_coef'])
        ## 
        ## 
        
        ######################################################################
        ######################################################################
        ######################################################################
        ## Utilizando a ANALYSIS(fuel_cool,emission_coef,water_use)
        ### COEFICIENTES PARA EL "FUEL_COOL"
        """
        def get_fuel_cool(df_abido_coef):  
            Parameters
            df_abido_coef: DataFrame
                Cost and emission coefficients from Abido (2003) paper
            df_coef: DataFrame
                Coefficients dataframe with fuel and cooling systems assigned
        
            # Local vars
            ## 
            ## Copia los coeficientes de ABIDO
            ##
            df_coef = df_abido_coef.copy()
            # Compute objectives terms
            ##
            ## SE ELIGE COMO VALOR 50.0 MW
            ##
            df_abido_coef['p_mw'] = 50.0
            df_objective_components = compute_objective_terms(df_abido_coef)
            #
            # Minimum emissions gets assigned nuclear
            ##
            ## Al sistema NUCLEAR SE LE ASIGNAN LOS VALORES MINIMOS DE EMISIONES
            ##
            nuc_idx = df_objective_components['F_emit'].idxmin()
            df_coef.loc[nuc_idx, 'fuel_type'] = 'Nuclear'
            df_coef.loc[nuc_idx, 'cooling_type'] = 'Once-through'
            #
            # Maximum emissions gets assigned coal
            ##
            ## SE ELIGEN LAS MAXIMAS EMISIONES PARA LOS CON CARBON
            ##
            coal_idx = df_objective_components['F_emit'].nlargest(2).index
            df_coef.loc[coal_idx, 'fuel_type'] = 'Coal'
            df_coef.loc[coal_idx, 'cooling_type'] = 'Once-through'
            #
            # Remaining assigned natural gas
            ##
            ## Los generadores a GAS se les ASIGNAN LOS VALORES NO USADOS (MINIMOS, MAXIMOS)
            ##
            df_coef['fuel_type'] = df_coef['fuel_type'].fillna('Natural Gas')
            # Natural gas get split to have some tower and some once-through cooling
            df_coef['cooling_type'] = df_coef['cooling_type'].fillna('Tower', limit=2)
            df_coef['cooling_type'] = df_coef['cooling_type'].fillna('Once-through')
            ##
            ## Retorna los coeficientes
            ##
            return df_coef
        """
        ## 
        # Fuel and cooling system type
        df_coef = analysis.get_fuel_cool(df_abido_coef)
        ######################################################################
        ######################################################################
        ######################################################################
        ### CASO PARA LAS EMISIONES 
        """       
        def get_emission_coef(df_coef):
            Overwrite emissions coefficients for nuclear generators
            Parameters
            df_coef: DataFrame
                Coefficients dataframe with fuel and cooling systems assigned
            Returns
            df_coef: DataFrame
                Updated coefficients dataframe with nuclear emission coefficients
                adjusted
        
            # Local vars
            ##
            ## Se asigna una lista con los coeficientes de contaminacion corespondientes
            ##
            emit_cols = ['alpha', 'beta_emit', 'gamma', 'xi', 'lambda']
            # Nuclear has no air pollution
            ##
            ## SE DESCARTA EL NUCLEAR PORQUE NO TIENE POLUCION
            ##
            nuc_idx = df_coef[df_coef['fuel_type'] == 'Nuclear'].index
            ##
            ## SE COLOCA EL NUCLEAR COMO EMISIONES Y POLUCION 0.0
            ##
            df_coef.loc[nuc_idx, emit_cols] = 0.0
            ##
            ## SE REPRESAN LOS COEFICINETES
            ##
            return df_coef        
        """        
        
        ##
        # Get emission coefficients
        df_coef = analysis.get_emission_coef(df_coef)
        ######################################################################
        ######################################################################
        ######################################################################
        
        ### CASO PARA EL USO DE AGUA ( MACKNICK)
        """
        def get_water_use_rate(df_coef, df_macknick_coef):
            Assign water consumption and withdrawal rates based on
            macknick_operational_2012
            @article{macknick_operational_2012,
                title = {Operational water consumption and withdrawal factors for
                electricity generating technologies: a review
                 of existing literature},
                doi = {10.1088/1748-9326},
                journal = {Environ. Res. Lett.},
                author = {Macknick, J and Newmark, R and Heath, G and Hallett, K C},
                year = {2012},
                pages = {11},}
            Parameters
            ----------
            df_coef: DataFrame
                Coefficients dataframe with fuel and cooling systems assigned
            df_macknick_coef: DataFrame
                Water use coefficients from Macknick (2012)
            Returns
            -------
            df_coef: DataFrame
                Coefficients dataframe with withdrawal and consumption rates assigned
                (gal/MWh)
                
            ##
            ## SE DEFINEN LOS COEFICIENTES DE WITHDRAWAL Y CONSUMPTION EN gal/MWh
            ##
            df_coef = pd.merge(
                                df_coef,
                                df_macknick_coef,
                                left_on=['fuel_type', 'cooling_type'],
                                right_on=['fuel_type', 'cooling_type']
                              )
            df_coef['beta_with'] = df_coef['withdrawal_rate_(gal/MWh)']
            df_coef['beta_con'] = df_coef['consumption_rate_(gal/MWh)']
        
            return df_coef        
        """        
        ##
        # Get water use coefficients
        df_coef = analysis.get_water_use_rate(df_coef, df_macknick_coef)
        
        
        ############################## FIN DE LAS ENTRADAS Y DEFINICIONES PARA EL COGIDO
        ######################################################################
        ######################################################################
        ######################################################################
        
        ######################################################################
        ######################################################################
        ######################################################################
        ################################################ INICIO DE TESTEO 
        # Testing of coefficients (useful for debugging)
        # df_coef['p_mw'] = 50.0
        # df_objective_components = analysis.compute_objective_terms(df_coef)
        # print(df_objective_components)
        
        
        
        ######################################################################
        ######################################################################
        ######################################################################        
        ######################## FIJAR LOS COEFICIENTES DE AL MODELO IEEE30
        ##
        ## COEFIECIENTES GUARDADOS Y ASIFGNADOS A LA NET 
        ##
        # Assign coefficients
        net.df_coef = df_coef


        ######################################################################
        ######################################################################
        ###################################################################### 
        ##
        ##
        ##
        ##
        """
        def get_generator_information(net, gen_types=('gen', 'sgen', 'ext_grid')):
            Get DataFrame summarizing all the generator information
            Parameters
            ----------
            net: pandapowerNet
                Network to summarize
            gen_types: list
                Generator types
            Returns
            -------
            df_gen_info: DataFrame
                Summary of generator information in `net`
            ##
            ## Se define un dataframe de pandas de la INFO DE LOS GENERADORES
            ##
            # Initialize local vars
            df_gen_info = pd.DataFrame()
            ##
            ## CONVERSION de los Generadores a un DATAFRAME 
            ##
            # Convert generator information dataframe
            for gen_type in gen_types:
                ##
                ## Se extrae la RED y el tipo de GENERADOR
                ##
                df_temp_gen_info = getattr(net, gen_type)
                ##
                ## Se extrae el TIPO DE GENERADOR
                ##
                df_temp_gen_info['et'] = gen_type
                ##
                ## Se definen por indice los elementos de los generadores
                ##
                df_temp_gen_info['element'] = df_temp_gen_info.index.astype(float)
                ##
                ## Se agregan la nueva info de los generadores 
                ##
                df_gen_info = df_gen_info.append(df_temp_gen_info)
            ##
            df_gen_info = df_gen_info.reset_index(drop=True)  # Drop duplicated indices

            return df_gen_info
        """
        # Sample decision space
        df_gen_info = analysis.get_generator_information(net)
        
        
        
        ## ???????????????
        ## DEFINIR UNA RED EXTERNA
        df_gen_info = \
            df_gen_info[df_gen_info['et'] != 'ext_grid']  # External grid


        ######################################################################
        ######################################################################
        ###################################################################### 
        ##
        ## 
        ## DATOS SOBRE el BUS, MIN, MAX y PASOS (especificaciones de la red)
        ##
        # solved during power flow
        df_gridspecs = pd.DataFrame({
                                      'var': df_gen_info['bus'].astype(int).tolist(),
                                      'min': df_gen_info['min_p_mw'].tolist(),
                                      'max': df_gen_info['max_p_mw'].tolist(),
                                      'steps': inputs['n_steps']
                                      })
        ######################################################################
        ##
        ## GRID SAMPLE
        ## SE ENTREGAN LAS ESPECIFICACIONES DE LA RED EN COLUMNAS 
        ##    VAR, MIN, MAX, STEPS
        """
        def grid_sample(df_gridspecs):
            Pandas-based grid sampling function
            Parameters
            ----------
            df_gridspecs: DataFrame
                Grid specifications, must have columns of
                ['var', 'min', 'max', 'steps']. 
                These reflect the 
                variable names,
                minimum value of grid sampling, 
                maximum value of grid sampling,
                and number of steps respectively.
            Returns
            -------
            df_grid: DataFrame
                Dataframe of grid sampling. 
                Will have columns of names specified in
                'var' list
            ----------------------
            #linear espaces para guardar los valores
            # Get linear spaces
            linspace_list = []
            #
            # Se agregan los valores de la red 
            for i, row in df_gridspecs.iterrows():
                linspace_list.append(
                                     np.linspace(row['min'], row['max'], int(row['steps']))
                                     )

            # Create dataframe
            #
            # SE crea el dataframe con los datos 
            df_grid = pd.DataFrame(
                                   list(itertools.product(*linspace_list)),
                                   columns=df_gridspecs['var'].tolist()
                                   )

            return df_grid
        """
        df_grid = analysis.grid_sample(df_gridspecs)
        print(f'Number of searches: {len(df_grid)}')
        
        ######################################################################
        ######################################################################
        ###################################################################### 
        ##
        ## RESOLVER EL MO-OPF
        ##
        # Solve opf for each grid entry
        ## Se define la red PARA INICIAR EL MO-OPF
        ddf_grid = dd.from_pandas(df_grid, npartitions=inputs['n_tasks'])
        
        
        
        ##
        ## funcion MULTI OBJETIVE OPTIMAL POWER FLOW 
        ##
        """
        def mo_opf(ser_decisions, net):
            --------------
            Multi-objective optimal power flow
            Parameters
            ----------
            ser_decisions: Series
                Decision variables in series, index is bus number, values are power in
                MW
            net: pandapowerNet
                Network to assess, df_coef attribute
            Returns
            -------
            ser_results: Series
                Series of objective values and internal decision
            ----------------
            ## COPIAR LA RED
            # Local vars
            net = copy.deepcopy(net)
            
            ## NOMBRES DE LAS DECISIONES 
            # Apply decision to network
            ser_decisions.name = 'p_mw_decisions'
            ##
            ## DECISIONES EN LOS BUS
            net.gen = net.gen.merge(ser_decisions, left_on='bus', right_index=True)
            ## DEFINIR LOS P_MW Y P_MW DECISIONS
            net.gen['p_mw'] = net.gen['p_mw_decisions']
            
            # Solve powerflow to solve for external generator
            ## RESOLVER CON PP (PandaPower) un flujo DC (DC POWER FLOW)
            pp.rundcpp(net)

            # Check if external generator is outside limits
            ## REVISAR SI LOS GENERADORES CUMPLEN CON SUS LIMITES 
            ## MIN < EXTERNAL GRID < MAX
            ##
            ## Valores de la red externa sacados de la NET
            ext_grid_p_mw = net.res_ext_grid['p_mw'][0]
            ##
            ## REVISAR LOS LIMITES DE LA RED EXTERNA
            ext_gen_in_limits = net.ext_grid['min_p_mw'][0] < ext_grid_p_mw < net.ext_grid['max_p_mw'][0]

            if ext_gen_in_limits:
                # Formatting results
                ## OBTENER LA INFORMACION DE LOS GENERADORES
                df_obj = get_generator_information(net, ['res_gen', 'res_ext_grid'])
                df_obj = df_obj.merge(
                                      net.df_coef,
                                      left_on=['element', 'et'],
                                      right_on=['element', 'et']
                                      )

                # Compute objectives terms
                ## DE LA LIBRERIA ANALISIS Y CORRESPONDIENTE CALCULO DE LAS FUNCIONES OBJETIVO
                df_obj = compute_objective_terms(df_obj)

                # Compute objectives
                ## DF_OBJ PRESENTA LAS CUATRO FUNCIONES CORRESPONDIENTES
                df_obj_sum = df_obj.sum()
                f_cos = df_obj_sum['F_cos']
                f_emit = df_obj_sum['F_emit']
                f_with = df_obj_sum['F_with']
                f_con = df_obj_sum['F_con']
                ser_results = pd.Series({
                                         0: ext_grid_p_mw,
                                        'F_cos': f_cos,
                                        'F_emit': f_emit,
                                        'F_with': f_with,
                                        'F_con': f_con
                                        })
            else:
                ser_results = pd.Series({
                                         0: np.nan,
                                         'F_cos': np.nan,
                                         'F_emit': np.nan,
                                         'F_with': np.nan,
                                         'F_con': np.nan
                                         })

            print(ser_results)

            return ser_results
        """
        ### CALCULO DEL MO-OPF
        df_grid_results = ddf_grid.apply(lambda row: analysis.mo_opf(row, net),
                                         axis=1,
                                         meta=pd.DataFrame(columns=[0]+obj_labs, dtype='float64')
                                         ).compute(scheduler='processes')
        ## CONCATENAR LOS RESULTADOS
        df_grid_results = pd.concat([df_grid, df_grid_results],
                                    axis=1
                                    )
        ######################################################################
        ######################################################################
        ######################################################################

        ## GUARDAR EN UN CSV LOS RESULTADOS DEL MO-OPF
        # Save checkpoint
        df_grid_results.to_csv(inputs['path_to_df_grid_results'], index=False)





    ### if not false:
    ###   print('hola')
    ### ---> hola
    ### if not True: 
    ### ---> None
        ######################################################################
        ######################################################################
        ###################################################################### 
    ## 
    ## 
    ## CON EL EXCEL DE LOS RESULTADOS DE LA RED ( ARRIBA) se clasifican las soluciones NO DOMINANTES
    ## 
    if not os.path.exists(inputs['path_to_df_nondom']):
        # Load required checkpoints
        ### SE REALIZA LA LECTURA DEL CSV CON LOS RESULTADOS DEL MOOPF
        df_grid_results = pd.read_csv(inputs['path_to_df_grid_results'])
        ## QUITAR TODOS LOS VALORES NULOS DEL EXCEL ( FILTRAR )
        # Drop na
        df_grid_results = df_grid_results.dropna()
        ##
        ##
        # Nondominated filter
        # FILTRO NO DOMINANTE
        ##
        """
        def get_nondomintated(df, objs, max_objs=None):
            ---------------------------------------------
            Get nondominated filtered DataFrame
            Parameters
            ----------
            df: DataFrame
                DataFrame for nondomination
            objs: list
                List of strings correspond to column names of objectives
            max_objs: list (Optional)
                List of objective to maximize
            Returns
            -------
            df_nondom: DataFrame
                Nondominatated DataFrame
            ------------------------------------------------
            # Create temporary dataframe for sorting
            df_sort = df.copy()

            # Flip objectives to maximize
            ## REORDENAR LOS OBJETIVOS PARAMAXIMIZAR
            if max_objs is not None:
                df_sort[max_objs] = -1.0 * df_sort[max_objs]

            # Non-dominated sorting
            # REALIZAR LA CLASIFICACION DE LOS NO DOMINANTES
            # USANDO LA FUNCION import pymoo.util.nds.efficient_non_dominated_sort as ends
            nondom_idx = ends.efficient_non_dominated_sort(df_sort[objs].values)
            ## ORDENAR EL DATAFRAME 
            df_nondom = df.iloc[nondom_idx[0]].sort_index()

            return df_nondom
            """
        """
        ## epsilon NO DOMINANTE
        def get_epsilon_nondomintated(df, objs, epsilons, max_objs=None):
            --------------------------------------------
            Get epsilon nondominated filtered DataFrame
            Parameters
            ----------
            df: DataFrame
                DataFrame for nondomination
            objs: list
                List of strings correspond to column names of objectives
            epsilons: list
                List of floats specifying epsilons for pareto sorting
            max_objs: list (Optional)
                List of objective to maximize
            Returns
            -------
            df_nondom: DataFrame
                Nondominatated DataFrame
            ---------------------------------------------
            ##
            ##
            ## indices de los OBJETIVOS
            # Get indices of objectives
            objs_ind = [df.columns.get_loc(col) for col in objs]
            try:
                max_ind = [df.columns.get_loc(col) for col in max_objs]
            except TypeError:
                max_ind = None

            # Nondominated sorting
            ## ORDEN NO DOMINANTE
            non_dominated = pto.eps_sort(
                                         [list(df.itertuples(False))], objs_ind, epsilons, maximize=max_ind
                                        )

            # To DataFrame
            ##
            ## Guardar los datos en un DATAFRAME
            df_nondom = pd.DataFrame(non_dominated, columns=df.columns)

            # Get original index
            ## RECUPERAR LOS INDICES
            df_nondom = df.reset_index().merge(df_nondom).set_index('index')
            df_nondom.index.name = None

            return df_nondom
        """
        
        
        ## Con RESULTADOS YA CALCULADOS, SE OBTIENEN LOS NO DOMINANTES
        df_nondom = analysis.get_epsilon_nondomintated(
                                                        df_grid_results,
                                                        objs=obj_labs,
                                                        epsilons=obj_epsi
                                                       )
        ###################################################################
        ###################################################################
        # Save checkpoint
        # SALVAR LOS VALORES FILTRADOS POR EL FILTRO NO DOMINANTE
        df_nondom.to_csv(inputs['path_to_df_nondom'], index=False)




    ### if not false:
    ###   print('hola')
    ### ---> hola
    ### if not True: 
    ### ---> None
    ###
        ######################################################################
        ######################################################################
        ###################################################################### 
    ##
    ## CON LOS RESULTADOS Y LOS EPSILON NO DOMINANTES se realiza una VISULAIZACION DE RESULTADOS
    ## 
    ##
    if not os.path.exists(inputs['path_to_nondom_objectives_viz']):
        # Load required checkpoints
        ## Cargar los datos NON DOMNINANTES
        df_nondom = pd.read_csv(inputs['path_to_df_nondom'])

        # Formatting
        ## ORDENAR LOS DATOS
        df_nondom = df_nondom.rename(
                                     dict(zip(dec_labs+obj_labs, dec_labs_pretty+obj_labs_pretty)),
                                          axis=1
                                     )
        ##
        ## COLOR INDEX PARA LOS COSTOS
        df_nondom['Color Index'] = df_nondom['Cost ($/hr)']
        ##
        ## GRADIENTE DE COLORES PARA LOS COSTOS 
        ## SE UTILIZAN LAS FUNCIONES DE "VIZ"
        ##
        """
        def set_color_gradient(df, colormap, label):
            # Compute Proportion of Each Line
            mixes = (df['Color Index'] - df['Color Index'].max()) /\
                    (df['Color Index'].min() - df['Color Index'].max())
            # Get Colors Corresponding to Proportions
            df['Color'] = [mpl.colors.rgb2hex(
                cm.get_cmap(colormap)(i)[:3]) for i in mixes]
            df['Color Label'] = label
            return df
        """
        
        ## GRADIENTE
        df_nondom = viz.set_color_gradient(
                                           df_nondom, 
                                           colormap='viridis', 
                                           label='Cost ($/hr)'
                                           )

        # Plot Objectives
        ## DEFINICIONES PARA PLOTEAR EN RANGOS
        ##
        ##
        ticks = [np.arange(0, 10000, 50),
                 np.arange(0.0, 1.0, 0.02),
                 np.arange(0, 10000000, 1000000),
                 np.arange(0, 300000, 20000)
                 ]
        
        ## DEFINICIONES PARA PLOTEAR EN RANGOS
        ##
        limits = [[590, 1050],
                  [0.17, 0.41],
                  [-1, 9100000],
                  [59000, 240010]
                  ]
        ##
        ## PLOTEAR CON STATIC PARALLEL
        """
        def static_parallel(df,
                            columns,
                            limits=None,
                            columns_to_flip=None,
                            n_ticks=6,
                            tick_precision=None,
                            explicit_ticks=None,
                            label_fontsize=12,
                            plot_legend=False,
                            plot_colorbar=False,
                            colorbar_colormap='viridis',
                            figure_size=(11, 3),
                            colorbar_adjust_args=(0.90, 0.2, 0.02, 0.70),
                            subplots_adjust_args={
                                'left': 0.05,
                                'bottom': 0.20,
                                'right': 0.85,
                                'top': 0.95,
                                'wspace': 0.0,
                                'hspace': 0.0},
                            rotate_x_labels=False):
            -----------------------------------------------------------------
            https://benalexkeen.com/parallel-coordinates-in-matplotlib/
            Coming soon, this will be turned into a proper module

            Parameters
            ----------
            df
            columns
            limits
            columns_to_flip
            n_ticks
            tick_precision
            explicit_ticks
            label_fontsize
            plot_legend
            plot_colorbar
            colorbar_colormap
            figure_size
            colorbar_adjust_args
            subplots_adjust_args
            rotate_x_labels

            Returns
            -------------------------------------------------------------------
            # Set automatic Values
            if limits is None:
                limits = list(zip(df[columns].min().values, df[columns].max().values))
            if tick_precision is None:
                tick_precision = [2]*len(columns)
            if isinstance(n_ticks, int):
                n_ticks = [n_ticks]*len(columns)
            if 'Linestyle' not in df.columns:
                df['Linestyle'] = '-'
            # Compute Numeric List of Columns
            x = [i for i, _ in enumerate(columns)]
            # Compute Indices of Columns to Flip
            try:
                flip_idx =\
                    [i for i, item in enumerate(columns) if item in columns_to_flip]
            except TypeError:
                flip_idx = [len(x) + 1]
            # Initialize Plots
            fig, axes = plt.subplots(
                1, len(columns) - 1, sharey=False, figsize=figure_size
            )
            if len(columns) == 2:
                axes = [axes]
            # Create Scaled DataFrame
            df_scaled = df.copy()
            for i, lim in enumerate(limits):
                lower = 0 + (df[columns[i]].min() - lim[0]) / (lim[1] - lim[0])
                upper = 0.0000001 + (df[columns[i]].max() - lim[0]) / (lim[1] - lim[0])
                scaler = MinMaxScaler(feature_range=(lower, upper))
                scaled_data = scaler.fit_transform(
                    df[columns[i]].values.reshape((-1, 1))
                )
                if i in flip_idx:
                    df_scaled[columns[i]] = 0.5 + (0.5 - scaled_data)
                else:
                    df_scaled[columns[i]] = scaled_data
            # Plot each row
            for i, ax in enumerate(axes):
                for idx in df_scaled.index:
                    ax.plot(
                        x,
                        df_scaled.loc[idx, columns],
                        df_scaled.loc[idx, 'Color'],
                        linestyle=df.loc[idx, 'Linestyle']
                    )
                ax.set_xlim([x[i], x[i + 1]])
                ax.set_ylim([0, 1])
            # Format Last Axis
            axes = np.append(axes, plt.twinx(axes[-1]))
            axes[-1].set_ylim(axes[-2].get_ylim())
            if rotate_x_labels:
                axes[-1].set_xticklabels(axes[-1].get_xticklabels(), rotation=90)
                axes[-2].set_xticklabels(axes[-2].get_xticklabels(), rotation=90)
            # Format All Axes
            for i, ax in enumerate(axes):
                format_ticks(
                    i,
                    ax,
                    n_ticks=n_ticks,
                    limits=limits,
                    cols=columns,
                    flip_idx=flip_idx,
                    x=x,
                    tick_precision=tick_precision,
                    explicit_ticks=explicit_ticks,
                    label_fontsize=label_fontsize,
                    df=df,
                    rotate_x_labels=rotate_x_labels
                )
            # Remove space between subplots
            plt.subplots_adjust(**subplots_adjust_args)
            # Add legend to plot
            if plot_legend:
                plt.legend(
                    [plt.Line2D((0, 1), (0, 0), color=i) for i in
                     df['Color'][df['Color Label'].drop_duplicates().index].values],
                    df['Color Label'][df['Color Label'].drop_duplicates().index],
                    bbox_to_anchor=(1.2, 1), loc=2, borderaxespad=0.0
                )
            if plot_colorbar:
                ax = plt.axes(colorbar_adjust_args)
                cmap = cm.get_cmap(colorbar_colormap)
                norm = mpl.colors.Normalize(
                    vmin=df['Color Index'].min(), vmax=df['Color Index'].max()
                )
                mpl.colorbar.ColorbarBase(
                    ax,
                    cmap=cmap.reversed(),
                    norm=norm,
                    orientation='vertical',
                    label=df.iloc[0]['Color Label']
                )
            plt.show()
            return fig
        """
        ##
        ## PLOTEO DE LOS OBEJTIVOS OBJS_LABS_PRETTY
        ##
        viz.static_parallel(
                            df=df_nondom,
                            columns=obj_labs_pretty,
                            plot_colorbar=True,
                            subplots_adjust_args={'left': 0.10,
                                                  'bottom': 0.20,
                                                  'right': 0.80,
                                                  'top': 0.95,
                                                  'wspace': 0.0,
                                                  'hspace': 0.0},
                            explicit_ticks=ticks,
                            limits=limits
                           ).savefig(inputs['path_to_nondom_objectives_viz'])

        # Plot Decisions
        ##
        ## PLOTEO DE LOS GENERADORES CON DEC_LABS_PRETTY 
        ##
        viz.static_parallel(
                            df=df_nondom,
                            columns=dec_labs_pretty,
                            plot_colorbar=True,
                            subplots_adjust_args={'left': 0.10,
                                                  'bottom': 0.20,
                                                  'right': 0.80,
                                                  'top': 0.95,
                                                  'wspace': 0.0,
                                                  'hspace': 0.0}
                            ).savefig(inputs['path_to_nondom_decisions_viz'])

            
        
    ### if not false:
    ###   print('hola')
    ### ---> hola
    ### if not True: 
    ### ---> None
    ###            
        ######################################################################
        ######################################################################
        ######################################################################         
    ##
    ##
    ## PLOTEO DE LAS CORRELACIONES 
    ##        
    if not os.path.exists(inputs['path_to_objective_correlation_viz']):
        # Load required checkpoints
        df_nondom = pd.read_csv(inputs['path_to_df_nondom'])

        # Formatting
        df_nondom = df_nondom.rename(dict(
                                          zip(dec_labs + obj_labs, dec_labs_pretty + obj_labs_pretty)),
                                     axis=1
                                     )
        
        
        ##
        ## Mapa de correlacion
        ##
        def correlation_heatmap(df):
            """
            ------------------------------------------------------------------
            Get correlation heatmap of objectives
            Parameters
            ----------
            df: DataFrame
                DataFrame to visualize
            Returns
            -------
            fig: matplotlib.figure.Figure
                Correlation heatmap figure
            -------------------------------------------------------------------
            """
            # Compute correlation
            df_corr = df.corr()

            # Get mask for lower triangle
            mask = np.triu(np.ones_like(df_corr,
                                        dtype=np.bool)
                           )
            np.fill_diagonal(mask, False)

            # Plot
            sns.set()
            g = sns.heatmap(
                            df_corr,
                            mask=mask,
                            vmin=-1,
                            vmax=1,
                            annot=True,
                            cmap='BrBG'
                            )
            plt.tight_layout()
            fig = g.figure

            # Show Plot
            plt.show()

            sns.reset_orig()

            return fig
        ## 
        ## OBTENER EL MAPA DE LAS CORRELACIONES  Y GUARDAR LA FIGURA 
        ##
        viz.correlation_heatmap(df_nondom[obj_labs_pretty]).savefig(inputs['path_to_objective_correlation_viz'])





    ### if not false:
    ###   print('hola')
    ### ---> hola
    ### if not True: 
    ### ---> None
    ###
        ######################################################################
        ######################################################################
        ###################################################################### 
    ##
    ## de ANALYSIS se extrae HITPLOT_PARALLEL
    ##
    ## 
    if not os.path.exists(inputs['path_to_nondom_hiplot_viz']):
        # Load required checkpoints
        df_nondom = pd.read_csv(inputs['path_to_df_nondom'])

            """
            def hiplot_parallel(df, invert_cols=None):
                -----------------------------------------------------------------------
                Create parallel plot using Hiplot
                Parameters
                ----------
                df: DataFrame
                    DataFrame to plot
                invert_cols: list
                    Columns to flip
                Returns
                -------
                exp: hiplot.experiment.Experiment
                    Hiplot parallel figure
                -----------------------------------------------------------------------
                exp = hip.Experiment.from_dataframe(df)
                exp.display_data(
                                 hip.Displays.PARALLEL_PLOT).update(
                                                                    {'hide': ['uid'], 'invert': invert_cols}
                                )
                exp.display_data(hip.Displays.TABLE).update({'hide': ['uid', 'from_uid']})
                print(f'Success: Created parallel plot of columns {df.columns.to_list()}')
                return exp
             """


        # Hiplot
        analysis.hiplot_parallel(df_nondom, invert_cols=None).to_html(inputs['path_to_nondom_hiplot_viz'])
    return 0




if __name__ == '__main__':
    main()

